import api from '../api'
import router from '../router'

export default {
  authenticate: async function (context, data) {
    try {
      const user = await api.user.login(data.user, data.password)
      const groups = await api.user.getGroups(data.user)
      context.commit('authenticated', { user, username: data.user })
      context.commit('setUserGroups', groups || [])
    } catch (e) {
      console.log(e)
      throw new Error(e)
    }
  },
  signup: async function (context, data) {
    try {
      let userObj = await api.user.signup(data)
      context.commit('setNewUser', { username: data.user, userObj })
    } catch (e) {
      throw new Error(e)
    }
  },
  verify: async function (context, data) {
    try {
      if (await api.user.verify(data.code, data.user)) {
        await context.dispatch('authenticate', data)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  signout: function (context) {
    api.user.signout(context.state.user)
    context.commit('signout')
  },
  checkDestination: function (context) {
    if (context.state.destination.page) {
      router.push(context.state.destination.page)
      context.commit('clearDestination')
      return true
    }
    return false
  },
  checkLogin: async function (context) {
    const validSession = await api.user.checkLogin()
    if (!validSession) {
      await api.user.checkLogin({allowUnauth: true})
      if (context.state.user.authenticated) context.commit('signout')
    }
    return validSession
  },
  ensureCredentials: async function (context) {
    return api.user.checkLogin({allowUnauth: true})
  },
  curatorCreateProduct: async function (context) {
    await api.user.curator.createProduct()
  },
  storeDestination: function (context, data) {
    context.commit('storeDestination', {
      page: data.path,
      type: data.name === 'Registry View' ? 'registry' : 'other',
      id: data.params.id || null
    })
  }
}
