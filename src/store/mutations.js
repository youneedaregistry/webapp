export default {
  authenticated (state, userData) {
    state.user.authenticated = true
    state.user.userObj = userData.user
    state.user.username = userData.username
    state.user.newUser = null
  },
  signout (state) {
    state.user.authenticated = false
    state.user.userObj = null
    state.user.username = null
    state.user.newUser = null
    state.user.userGroups = []
  },
  setNewUser (state, newUser) {
    state.user.newUser = newUser.username
    state.user.userObj = newUser.userObj
  },
  setUserGroups (state, groups) {
    state.user.userGroups = groups
  },
  storeDestination (state, data) {
    state.destination.page = data.page
    state.destination.type = data.type
    state.destination.id = data.id
  },
  clearDestination (state) {
    state.destination.page = null
    state.destination.type = null
    state.destination.id = null
  }
}
