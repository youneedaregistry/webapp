export default {
  isInGroup (state) {
    return group => {
      return state.user.userGroups.includes(group)
    }
  }
}
