export default {
  user: {
    authenticated: false,
    userObj: null,
    username: null,
    newUser: null,
    userGroups: []
  },
  destination: {
    page: null,
    type: null,
    id: null
  }
}
