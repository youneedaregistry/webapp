const FeatureDetection = {
  install (Vue, options) {
    let vm = new Vue({
      data: {
        features: {
          touch: false
        }
      }
    })
    window.addEventListener('touchstart', () => {
      vm.features.touch = true
    })
    Vue.prototype.$fd = vm.features
  }
}

export default FeatureDetection
