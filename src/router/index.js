import Vue from 'vue'
import Router from 'vue-router'
import SignIn from '@/components/pages/SignIn'
import Register from '@/components/pages/Register'
import Verify from '@/components/pages/Verify'
import Home from '@/components/pages/Home'
import RegistryCreate from '@/components/pages/RegistryCreate'
import RegistryEdit from '@/components/pages/RegistryEdit'
import CuratorDashboard from '@/components/pages/CuratorDashboard'
import CuratorNewProduct from '@/components/pages/CuratorNewProduct'
import CuratorProductAdded from '@/components/pages/CuratorProductAdded'
import RegistryView from '@/components/pages/RegistryView'

import { requireUnauth, requireAuth, requireCreds } from './authGuard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    requireCreds({
      path: '/',
      name: 'Home',
      component: Home
    }),
    requireUnauth({
      path: '/signin',
      name: 'Sign In',
      component: SignIn
    }),
    requireUnauth({
      path: '/register',
      name: 'Register',
      component: Register
    }),
    requireUnauth({
      path: '/verify',
      name: 'Verify Account',
      component: Verify
    }),
    requireUnauth({
      path: '/verify/:code',
      name: 'Verify Account With Code',
      component: Verify
    }),
    requireAuth({
      path: '/registry/create',
      name: 'Create Registry',
      component: RegistryCreate
    }),
    requireAuth({
      path: '/registry/edit/:id',
      name: 'Edit Registry',
      component: RegistryEdit
    }),
    requireAuth({
      path: '/curator',
      name: 'Curator Dashboard',
      component: CuratorDashboard
    }),
    requireAuth({
      path: '/curator/new',
      name: 'Curator New Product',
      component: CuratorNewProduct
    }),
    requireAuth({
      path: '/curator/added/:id',
      name: 'Curator Product Added',
      component: CuratorProductAdded
    }),
    requireAuth({
      path: '/:id',
      name: 'Registry View',
      component: RegistryView
    })
  ]
})
