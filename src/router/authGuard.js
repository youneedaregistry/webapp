import store from '../store'

export function requireAuth (route) {
  route.beforeEnter = async function (to, from, next) {
    if (!await store.dispatch('checkLogin')) {
      store.dispatch('storeDestination', to)
      next('/signin')
      return
    }
    next()
  }
  return route
}

export function requireUnauth (route) {
  route.beforeEnter = async function (to, from, next) {
    if (await store.dispatch('checkLogin')) {
      next('/')
      return
    }
    next()
  }
  return route
}

export function requireCreds (route) {
  route.beforeEnter = async function (to, from, next) {
    if (!await store.dispatch('ensureCredentials')) {
      next('/')
      return
    }
    next()
  }
  return route
}
