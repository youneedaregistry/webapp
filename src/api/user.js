import config from './config'
import aws from './aws'
import {
  CognitoUserPool,
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute
} from 'amazon-cognito-identity-js'

const getCognitoUser = () => {
  const userPool = new CognitoUserPool({
    UserPoolId: config.cognito.USER_POOL_ID,
    ClientId: config.cognito.APP_CLIENT_ID
  })
  return userPool.getCurrentUser()
}

const getUnauthenticatedCognitoUser = (Username) => {
  const Pool = new CognitoUserPool({
    UserPoolId: config.cognito.USER_POOL_ID,
    ClientId: config.cognito.APP_CLIENT_ID
  })
  const userData = { Username, Pool }
  return new CognitoUser(userData)
}

const getSession = (user) => {
  const cognitoUser = user || getCognitoUser()
  return new Promise((resolve, reject) => {
    cognitoUser.getSession((err, session) => {
      if (err) {
        reject(err)
        return
      }

      resolve(session)
    })
  })
}

const getUserToken = (user) => {
  return new Promise((resolve, reject) => {
    getSession(user).then(session => {
      resolve(session.getIdToken().getJwtToken())
    }).catch(err => {
      reject(err)
    })
  })
}

const checkLogin = async function (opts = {}) {
  const currentUser = getCognitoUser()

  if (currentUser === null && opts.allowUnauth !== true) {
    return false
  }

  try {
    if (currentUser) {
      const userToken = await getUserToken(currentUser)
      await aws.getAwsCredentials(userToken)
    } else {
      await aws.getAwsCredentials()
    }
    return true
  } catch (e) {
    return false
  }
}

export default {
  login (user, password) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    })
    const cognitoUser = new CognitoUser({ Username: user, Pool: userPool })
    const authenticationData = { Username: user, Password: password }
    const authenticationDetails = new AuthenticationDetails(authenticationData)

    return new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: result => resolve(cognitoUser),
        onFailure: err => reject(err)
      })
    })
  },

  signup (data) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    })

    const attributes = [
      new CognitoUserAttribute({ Name: 'name', Value: data.name })
    ]

    return new Promise((resolve, reject) => {
      userPool.signUp(data.user, data.password, attributes, null, (err, result) => {
        if (err) {
          reject(err)
          return
        }

        resolve(result.user)
      })
    })
  },

  verify (code, username) {
    const cognitoUser = getUnauthenticatedCognitoUser(username)

    return new Promise((resolve, reject) => {
      cognitoUser.confirmRegistration(code, true, (err, result) => {
        if (err) {
          reject(err)
          return
        }

        resolve(result)
      })
    })
  },

  signout () {
    const cognitoUser = getCognitoUser()

    cognitoUser.signOut()
  },

  getGroups: async function () {
    const session = await getSession()
    return session.getIdToken().payload['cognito:groups']
  },

  checkLogin,

  curator: {
    createProduct: async function (product) {
      if (!await checkLogin()) {
        throw new Error('You do not have permission to do this!')
      }

      try {
        return await aws.invokeAPI({
          path: '/products',
          method: 'POST',
          body: product
        })
      } catch (e) {
        console.log(e)
      }
    }
  }
}
