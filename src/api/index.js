import config from './config'
import user from './user'
import products from './products'
import registries from './registries'

export default {
  config,
  user,
  products,
  registries
}
