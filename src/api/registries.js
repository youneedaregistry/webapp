import aws from './aws'

export default {
  create: async function (name, description, url) {
    return aws.invokeAPI({
      path: '/registries/create',
      method: 'POST',
      body: {name, description, url},
      gateway: 'base'
    })
  },

  get: async function (url) {
    return aws.invokeAPI({
      path: `/registries/get/${url}`,
      method: 'GET',
      gateway: 'base'
    })
  },

  addProduct: async function (url, product) {
    return aws.invokeAPI({
      path: '/registries/add-product',
      method: 'POST',
      body: {url, product},
      gateway: 'base'
    })
  },

  deleteProduct: async function (url, pid) {
    return aws.invokeAPI({
      path: '/registries/delete-product',
      method: 'POST',
      body: {url, pid},
      gateway: 'base'
    })
  },

  updateProductQty: async function (url, pid, qty) {
    return aws.invokeAPI({
      path: '/registries/update-product-qty',
      method: 'POST',
      body: {url, pid, qty},
      gateway: 'base'
    })
  },

  updateName: async function (url, name, description) {
    return aws.invokeAPI({
      path: '/registries/update-name',
      method: 'POST',
      body: {url, name, description},
      gateway: 'base'
    })
  },

  markProductPurchased: async function (url, pid, email) {
    return aws.invokeAPI({
      path: '/registries/mark-product-purchased',
      method: 'POST',
      body: {url, pid, email},
      gateway: 'base'
    })
  },

  markPurchaseConfirmed: async function (purchasedId) {
    return aws.invokeAPI({
      path: '/registries/mark-purchase-confirmed',
      method: 'POST',
      body: {purchasedId},
      gateway: 'base'
    })
  },

  deletePurchaseRecord: async function (purchasedId) {
    return aws.invokeAPI({
      path: '/registries/delete-purchase-record',
      method: 'POST',
      body: {purchasedId},
      gateway: 'base'
    })
  },

  getPurchasedProducts: async function (email, url) {
    return aws.invokeAPI({
      path: '/registries/get-purchased-products',
      method: 'POST',
      body: {email, url},
      gateway: 'base'
    })
  },

  testURL: async function (url) {
    return aws.invokeAPI({
      path: '/registries/testurl',
      method: 'POST',
      body: {url},
      gateway: 'base'
    })
  },

  addToHistory: async function (url, name) {
    return aws.invokeAPI({
      path: '/registries/add-to-history',
      method: 'POST',
      body: {url, name},
      gateway: 'base'
    })
  },

  getHistory: async function () {
    return aws.invokeAPI({
      path: '/registries/get-history',
      method: 'POST',
      body: {},
      gateway: 'base'
    })
  }
}
