import aws from './aws'

export default {
  lookupAmazon: async function (itemId) {
    return aws.invokeAPI({
      path: '/products/lookup',
      method: 'POST',
      body: {itemId},
      gateway: 'base'
    })
  },

  lookup: async function (url) {
    return aws.invokeAPI({
      path: '/products/lookup',
      method: 'POST',
      body: {url},
      gateway: 'base'
    })
  },

  get: async function (productId) {
    return aws.invokeAPI({
      path: `/products/${productId}`,
      method: 'GET',
      gateway: 'base'
    })
  }
}
