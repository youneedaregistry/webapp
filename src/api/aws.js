import AWS from 'aws-sdk'
import config from './config'
import sigV4Client from './sigV4Client'

export default {
  getAwsCredentials (userToken) {
    if (userToken) {
      const authenticator = `cognito-idp.${config.cognito.REGION}.amazonaws.com/${config.cognito.USER_POOL_ID}`

      AWS.config.update({ region: config.cognito.REGION })

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: config.cognito.IDENTITY_POOL_ID,
        Logins: {
          [authenticator]: userToken
        }
      })
    } else {
      AWS.config.update({ region: config.cognito.REGION })

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: config.cognito.IDENTITY_POOL_ID
      })
    }

    return AWS.config.credentials.getPromise()
  },

  credentialsValid () {
    return AWS.config.credentials &&
      !AWS.config.credentials.expired &&
      Date.now() < AWS.config.credentials.expireTime - 60000
  },

  invokeAPI: async function ({path, method = 'GET', headers = {}, queryParams = {}, body, gateway = 'curator'}) {
    const signedRequest = sigV4Client.newClient({
      accessKey: AWS.config.credentials.accessKeyId,
      secretKey: AWS.config.credentials.secretAccessKey,
      sessionToken: AWS.config.credentials.sessionToken,
      region: config.apiGateway[gateway].REGION,
      endpoint: config.apiGateway[gateway].URL
    }).signRequest({
      method,
      path,
      headers,
      queryParams,
      body
    })

    body = body ? JSON.stringify(body) : body
    headers = signedRequest.headers

    const results = await fetch(signedRequest.url, {
      method,
      headers,
      body
    })

    if (results.status !== 200) {
      throw new Error(await results.text())
    }

    return results.json()
  }
}
