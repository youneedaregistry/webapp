export default {
  cognito: {
    USER_POOL_ID: 'us-west-2_VjW7dwkFN',
    APP_CLIENT_ID: '2nofmkadgn0rjacng17tflqb5s',
    REGION: 'us-west-2',
    IDENTITY_POOL_ID: 'us-west-2:2ad19132-44e3-48ad-8f1c-fce3ac53176c'
  },
  apiGateway: {
    curator: {
      REGION: 'us-west-2',
      URL: 'https://dzdvgk1cri.execute-api.us-west-2.amazonaws.com/prod'
    },
    base: {
      REGION: 'us-west-2',
      URL: 'https://0lvsz6jnhd.execute-api.us-west-2.amazonaws.com/prod'
    }
  }
}
